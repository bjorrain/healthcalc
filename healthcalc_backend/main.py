from flask import Flask, jsonify, request
from basic import basic_calc
import logging

logging.basicConfig(level=logging.INFO)

app = Flask(__name__)
app.register_blueprint(basic_calc)
CALC_LIST = {"basic/bmi": {"name": "имт", "desc": "Индекс Массы Тела", "url": "/api/calc/basic/bmi"},
             "heart/pulse": {"name": "пульс", "desc": "", "url": "/api/calc/heart/pulse"}}




@app.route("/api/calc/list", methods=["GET"])
def calcs_list():
    return jsonify(CALC_LIST)

if __name__ == "__main__":
    app.run(debug=True, port=8080)

