from flask import Blueprint, jsonify, request, abort
import logging
import json
NAME = "name"
NUMBER = "number"
STRING = "string"
FIELDS = "fields"
TYPE = "type"
CHOICES = "choices"
IMAGES = "images"
TIMERS = "timers"

basic_calc = Blueprint("basic", __name__, url_prefix="/api/calc/basic")


@basic_calc.route("/", methods=["GET"])
def list_basics():
    abort(400)

@basic_calc.route("/bmi", methods=["GET", "POST"])
def get_info_of_bmi():
    print("in bmi function!")
    print(request.method)
    print(request.data)
    if request.method == "GET":
        return jsonify(
        {
            NAME: "ИМТ", 
            FIELDS: [
                {
                    NAME: "height",
                    TYPE: NUMBER,
                    IMAGES: [],
                },
                {
                    NAME: "weight",
                    TYPE: NUMBER,
                    IMAGES: [],
                }
            ]
        }
        )
    else: 
        print(request.data)
        rq = json.loads(request.json)
        height = int(rq["height"]) 
        weight = int(rq["weight"])
        bmi = weight / (height / 100) ** 2
        score = score = 10 * (1 - abs((bmi / 21.75) - 1))
        return jsonify({"result": bmi, "score": score})

