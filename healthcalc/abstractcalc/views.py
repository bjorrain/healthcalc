from django.shortcuts import render
from rest_framework.decorators import api_view
from rest_framework.response import Response

# Create your views here.



@api_view
def api_list(request):
    CALC_LIST = {
    "Пульс": "/api/calc/heart/pulse",
    "Индекс массы тела": "/api/calc/bmi",
}

    return Response(CALC_LIST)