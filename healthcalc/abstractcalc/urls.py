from django.urls import path
from bmicalc.views import api_bmi
from .views import api_list
urlpatterns = [
    path("bmi", api_bmi),
    path("", api_list),
]
