from django.apps import AppConfig


class AbstractcalcConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'abstractcalc'
