from django.contrib.auth.forms import UserCreationForm, UserChangeForm, SetPasswordForm
from .models import User
from django import forms
from django.utils.translation import gettext_lazy as _


class CreateUserForm(UserCreationForm):
    email = forms.EmailField()
    first_name = forms.CharField(max_length=30, required=True)
    last_name = forms.CharField(max_length=30, required=True)
    birth_date = forms.DateField(required=True)
    gender = forms.ChoiceField(choices=[("M", _("male")), ("F", _("female"))])

    class Meta:
        model = User
        fields = (_('email'), _("first_name"), _("last_name"), _("birth_date"), _("gender"),
                  _('password1'), _('password2'))

class ChangeUserForm(UserChangeForm):
    email = forms.EmailField()
    first_name = forms.CharField(max_length=30, required=True)
    last_name = forms.CharField(max_length=30, required=True)
    birth_date = forms.DateField(required=True)
    gender = forms.ChoiceField(choices=[("M", _("male")), ("F", _("female"))])

    class Meta:
        model = User
        fields = ('email', "first_name", "last_name", "birth_date", "gender")

class UpdateUserForm(forms.ModelForm):
    email = forms.EmailField()
    first_name = forms.CharField(max_length=30, required=True)
    last_name = forms.CharField(max_length=30, required=True)
    birth_date = forms.DateField(required=True)
    gender = forms.ChoiceField(choices=[("M", _("male")), ("F", _("female"))])
    class Meta:
        model = User
        fields = ('email', "first_name", "last_name", "birth_date", "gender")

class UpdatePasswordForm(SetPasswordForm):
    new_password1 = forms.CharField(
        label=_("New password"),
        widget=forms.PasswordInput(attrs={'autocomplete': 'new-password'}),
    )
    new_password2 = forms.CharField(
        label=_("New password confirmation"),
        widget=forms.PasswordInput(attrs={'autocomplete': 'new-password'}),
    )