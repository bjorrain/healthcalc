from django.db import models
from django.contrib.auth.models import AbstractUser
import uuid
from django.utils.translation import gettext_lazy as _
# Create your models here.


class User(AbstractUser):

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

    username = models.CharField(_('username'),primary_key=False, max_length=150, unique=True)

    email = models.EmailField(max_length=100, unique=True)

    first_name = models.CharField(_('first name'),max_length=30)

    last_name = models.CharField(_('last name'),max_length=30)

    birth_date = models.DateField(_('birth date'))

    gender = models.CharField(_('gender'),max_length=1, choices=(("M", _("male")), ("F", _("female"))))

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = ["username", "first_name", "last_name", "birth_date", "gender"]
