from django.contrib import admin
from .models import User
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from .forms import CreateUserForm, ChangeUserForm

# Register your models here.


class HCUserAdmin(BaseUserAdmin):

    class Meta:
        model = User
        fields = ("email", "username", "first_name", "last_name", "birth_date",
                  "gender")
        add_form = CreateUserForm
        change_form = ChangeUserForm

    list_display = ("email", "username", "first_name", "last_name",
                    "birth_date", "gender", "id")
    search_fields = ("email", "first_name", "last_name", "username")
    add_fieldsets = ((
                None,
                {
                    'classes': ('wide',),
                    'fields': ("email", "username", "first_name", "last_name",
                    "birth_date", "gender", 'password1', 'password2'),
                },
            ),
        )



admin.site.register(User, HCUserAdmin)
