from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import messages, auth
from .forms import CreateUserForm
# Create your views here.

def sign_up(request):
    if request.method == 'POST':
        form = CreateUserForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('login')
    else:
        form = CreateUserForm()
    return render(request, 'registration/register.html', {'form': form})