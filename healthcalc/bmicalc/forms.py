from django import forms
from django.utils.translation import gettext_lazy as _

class BMI_Form(forms.Form):
    class Meta:
        fields = ["height", "weight"]
    height = forms.IntegerField()
    weight = forms.IntegerField()
