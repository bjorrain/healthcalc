from rest_framework import serializers     


API_INFO = {
    "name": "ИМТ",
    "description": "Индекс массы тела",
    "api_url": "/api/calc/bmi",
    "fields": [
        {
            "name": "height",
            "description": "Рост",
            "type": "number"
        },
        {
            "name": "weight",
            "description": "Вес",
            "type": "number"
        }
    ],
    "timers": [],
    "images": []
}



def calc_bmi(height: float, weight: float):
    bmi = weight / height ** 2
    if bmi < 1:
        bmi *= 10000
    score = 10 * (1 - abs((bmi / 21.75) - 1))
    return {
        "result": bmi,
        "score": score
    }

class BMI_Serializer(serializers.Serializer):
    height = serializers.FloatField()
    weight = serializers.FloatField()

class BMI_Serializer_Response(serializers.Serializer):
    result = serializers.FloatField()
    score = serializers.FloatField()