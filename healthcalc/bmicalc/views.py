from mimetypes import init
from django.shortcuts import render
from regex import R
from .forms import BMI_Form
from django.utils.translation import gettext_lazy as _
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .api import calc_bmi, API_INFO

# Create your views here.

def calculate_bmi(request):
    bmi = _("Fill form first")
    result_expl = _("Fill form first")
    score = 0
    
    try:
        initial = {
            "height": request.COOKIES.get("height"),
            "weight": request.COOKIES.get("weight")
        }
    except Exception:
        initial = None
    
    bmiform = BMI_Form(initial=initial)
    
    if request.POST:
        height = int(request.POST.get("height"))
        weight = int(request.POST.get("weight"))
    elif initial["height"].isdigit() and initial["weight"].isdigit():
        height, weight = int(initial["height"]), int(initial["weight"])
    else:
        height, weight = 0, 0
    
    if height and weight:
        bmi = weight / (height ** 2)
        
        if bmi < 1:
            bmi *= 10000
        
        if 18.5 <= bmi <= 25:
            result_expl = _("normal")
        elif bmi > 25:
            result_expl = _("overweight")
        else:
            result_expl = _("low")
        
        score = 10 * (1 - abs((bmi / 21.75) - 1))
        
        rsp = render(
            request, 
            "calc.html", 
            {
                "form": bmiform, 
                "result": bmi, 
                "result_expl": result_expl, 
                "score": str(score) if score else "Fill form first"
            }
        )
        
        rsp.set_cookie("bmi", str(bmi))
        rsp.set_cookie("result_expl", result_expl)
        rsp.set_cookie("bmi_score", str(score))
        rsp.set_cookie("height", str(request.POST.get("height")))
        rsp.set_cookie("weight", str(request.POST.get("weight")))
        
        return rsp
    
    return render(
        request, 
        "calc.html", 
        {
            "form": bmiform, 
            "result": bmi, 
            "result_expl": result_expl, 
            "score": str(score) if score else _("Fill form first"),
            "url_name": "/calc/bmi"
        }
    )

@api_view(["GET", "POST"])
def api_bmi(request):
    if request.method == "GET":
        return Response(API_INFO)
    elif request.method == "POST":
        height = int(request.data.get("height"))
        weight = int(request.data.get("weight"))
        return Response(calc_bmi(height, weight))