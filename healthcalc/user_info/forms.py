from django import forms
from django.utils.translation import gettext_lazy as _
from .models import UserHealth

class UserHealthForm(forms.ModelForm):
    class Meta:
        model = UserHealth
        fields = ['height', 'body_type', 'blood_pressure_type']
    def __init__(self, user, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['body_type'].widget.attrs.update({'class': 'form-control'})
        self.fields['blood_pressure_type'].widget.attrs.update({'class': 'form-control'})
        user = user