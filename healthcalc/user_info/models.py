from django.db import models

# Create your models here.


class UserHealth(models.Model):
    user = models.ForeignKey("accounts.User", on_delete=models.CASCADE)
    height = models.IntegerField()
    body_type = models.CharField(choices=[("S", "эктоморф"), ("N", "мезоморф"),
                                          ("F", "эндоморф")],
                                 max_length=1)

    blood_pressure_type = models.CharField(choices=[("U", "гипотония"),
                                                    ("N", "норма"),
                                                    ("O", "гипертония")],
                                           max_length=1)
    def __str__(self):
        return str(self.user.__dict__)
