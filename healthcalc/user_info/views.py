
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from accounts.forms import UpdateUserForm, UpdatePasswordForm
from .forms import UserHealthForm

# Create your views here.

@login_required
def profile(request):
    initial = request.user._wrapped
    userform = UpdateUserForm(initial=initial.__dict__)
    passwordform = UpdatePasswordForm(initial)
    if request.user.userhealth_set.all():
        healthform = UserHealthForm(initial=request.user.userhealth_set.first().__dict__, user=request.user)
    else:
        healthform = UserHealthForm(user=request.user)
    if request.method == 'POST':
        userform = UpdateUserForm(request.POST, instance=request.user)
        passwordform = UpdatePasswordForm(request.POST)
        healthform = UserHealthForm(request.POST, instance=request.user.userhealth_set.first())
        if userform.is_valid():
            userform.save()
        if passwordform.is_valid():
            passwordform.save()
        if healthform.is_valid():
            healthform.save()
    return render(request, 'registration/profile.html', {"userform":userform, "passwordform":passwordform, "healthform":healthform})

