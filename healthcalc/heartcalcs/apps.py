from django.apps import AppConfig


class HeartcalcsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'heartcalcs'
