from django.shortcuts import render
from .forms import PulseForm
from django.utils.translation import gettext_lazy as _


def pulse(request):
    result_expl = _("Fill form first")
    score = 0
    
    try:
        initial = {
            "pulse": request.COOKIES.get("pulse"),
            "gender": request.COOKIES.get("gender"),
            "age": request.COOKIES.get("age"),
        }
    except Exception:
        initial = None
    form = PulseForm(initial=initial)
    if request.POST:
        pulse = int(request.POST.get("pulse"))
        gender = request.POST.get("gender")
        age = int(request.POST.get("age"))
    elif initial:
        pulse, gender, age = initial["pulse"], initial["gender"], initial["age"]
    else: 
        pulse, gender, age = 0, "m", 0
    if not (pulse and age):
        score = 0
    match gender:
        case "m":
            if 17 <= age < 26:
                score = 10 * (48 / pulse)
            elif 26 <= age < 36:
                score = 10 * (49 / pulse)
            elif 46 <= age < 56:
                score = 10 * (49 / pulse)
            elif 56 <= age < 56:
                score = 10 * (50 / pulse)
            else:
                score = 10 * (49 / pulse)
        case "f":
            if 17 <= age < 26:
                score = 10 * (53 / pulse)
            elif 26 <= age < 36:
                score = 10 * (53 / pulse)
            elif 46 <= age < 56:
                score = 10 * (53 / pulse)
            elif 56 <= age < 56:
                score = 10 * (53 / pulse)
            else:
                score = 10 * (53 / pulse)
        case _:
            result_expl = _("Please select gender")

    result_expl = _("отлично") if score > 0.85 else _("очень хорошо") \
        if score > 0.77 else _("хорошо") if score > 0.72 \
        else _("удовлетворительно") if score > 0.58 else _("неудовлетворительно")
    
    rsp = render(request, 'calc.html', {'form': form, "result_expl": result_expl, "score": score})
    rsp.set_cookie("pulse", str(pulse))
    rsp.set_cookie("result_expl", result_expl)
    rsp.set_cookie("pulse_score", str(score))
    rsp.set_cookie("gender", gender)
    rsp.set_cookie("age", str(age))
    return render(request, 'calc.html', {'form': form, "result_expl": result_expl, "score": score, "result": str(pulse), "url_name": "/calc/heart/pulse"})

# Create your views here.
