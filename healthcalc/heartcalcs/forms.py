from django import forms 
from django.utils.translation import gettext_lazy as _

class PulseForm(forms.Form):
    pulse = forms.IntegerField()
    age = forms.IntegerField()
    gender = forms.ChoiceField(choices=[("m", _("male")), ("f", _("female"))])