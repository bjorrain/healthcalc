from django.shortcuts import render

# Create your views here.


def mainpage(request):
    return render(request, 'mainpage.html')

def about(request):
    return render(request, 'about.html')

def calclist(request):
    calcs = {
        "Индекс массы тела": "/calc/bmi",
        "Пульс": "/calc/heart/pulse",
#        "Давление": "/calc/heart/pressure",
#        "Степ-тест": "/calc/heart/steptest",
    }
    return render(request, 'calclist.html', {"object_list":calcs})