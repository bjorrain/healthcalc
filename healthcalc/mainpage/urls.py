from django.urls import path
import mainpage.views as views

urlpatterns = [
    path('', views.mainpage),
    path('about/', views.about),
    path('calc/', views.calclist),
    
]