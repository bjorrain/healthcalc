import logging
from time import sleep
from typing import Optional
import flet
import requests
from threading import Thread
import json

def make_button(text: str, url: str, page: flet.Page):
    return flet.ElevatedButton(text, on_click= lambda _ : page.go(url))

def go(page, url):
    page.route = url
    page.update()

def make_field(ftype: str, fname: str, page: flet.Page, choices: Optional[list] = None, time: Optional[int] = None):
    if ftype == "number":
        return flet.TextField(label=fname, input_filter=flet.NumbersOnlyInputFilter(), keyboard_type=flet.KeyboardType.NUMBER, adaptive=True, on_change=lambda e: set_storage(page, fname, int(e.control.value)))
    if ftype == "string":
        return flet.TextField(label=fname, adaptive=True, on_change=lambda e: set_storage(page, fname ,e.control.value))
    if ftype == "choices":
            return flet.Dropdown(label=fname, options=[flet.dropdown.Option(i) for i in choices], on_change=lambda e: set_storage(page, fname, e.control.value))
    if ftype == "timer":
        return make_timer(page, time)

def mainpage(page: flet.Page, appbar: flet.AppBar, navbar: flet.NavigationBar):
    view = flet.View("/", 
                     [flet.ResponsiveRow(
                     [flet.Text("Калькулятор Здоровья."),
                     make_button("Список калькуляторов", "/calclist", page),
                     ], alignment=flet.MainAxisAlignment.CENTER)],
                    appbar=appbar,
                    navigation_bar=navbar
                    )
    return view

def listpage(page: flet.Page, appbar: flet.AppBar, navbar: flet.NavigationBar):
    controls = []
    rsp = requests.get("http://localhost:8080/api/calc/list").json()
    for i in rsp.keys():
        controls.append(make_button(rsp[i]["name"], f"/calc/{i}", page))

        logging.info(f"{i}: {rsp[i]['name']}: {rsp[i]['url']}")
    view = flet.View("/calclist", 
                     controls=[flet.ResponsiveRow(controls, alignment=flet.MainAxisAlignment.CENTER)],
                    appbar=appbar,
                    navigation_bar=navbar
                    )
    return view

def make_timer(page: flet.Page, time: int):
    c = []
    c.append(flet.ProgressBar(value=0))
    c.append(flet.ElevatedButton("Start", on_click=lambda _: Thread(target=wait, args=(time, c[0])).start()))
    controls = flet.Column(c)
    return controls

def set_storage(page: flet.Page, key: str, value):
    logging.info(f"{key} : {str(value)}")
    page.client_storage.set(key, value)
    logging.info(f"set {key} : {page.client_storage.get(key)}")

def get_storage(page:flet.Page, key: str):
    logging.info(f"{key} : {page.client_storage.get(key)}")
    return page.client_storage.get(key)

def wait(time: int, control: flet.ProgressBar, text: flet.Text):
    for i in range(time):
        sleep(1)
        control.value = i / time
        text.value = f"Осталось {time - i} секунд" 

def inputpage(page: flet.Page, url: str, appbar: flet.AppBar, navbar: flet.NavigationBar):
    rsp = requests.get(f"http://localhost:8080/api/calc/{url}").json()
    controls = []
    for i in rsp["fields"]:
        controls += [make_field(i["type"], i["name"], page, i["choices"] if "choices" in i.keys() else None, i["time"] if "time" in i.keys() else None)]
    controls.append(flet.ElevatedButton("Сохранить", on_click=lambda _: go(page, f"/result/{url}")))
    set_storage(page, f"""{url.split("/")[1]}_fields""", [i["name"] for i in rsp["fields"]])
    view = flet.View(f"/calc/{url}", 
                     controls=controls,
                    appbar=appbar,
                    navigation_bar=navbar
                    )
    return view
    
def outputpage(page: flet.Page, url: str, appbar: flet.AppBar, navbar: flet.NavigationBar):
    data = {}
    controls = []
    for i in get_storage(page=page, key=f"{url.split('/')[1]}_fields"):
        logging.error(i)
        logging.error(str(data))
        data[i] = page.client_storage.get(i)
    logging.info(data)
    logging.info(f"http://localhost:8080/api/calc/{url}")
    rsp = requests.post(f"http://localhost:8080/api/calc/{url}", json=json.dumps(data))
    logging.info(rsp)
    rsp = rsp.json()
    result, score = rsp["result"], rsp["score"]
    if 8 <= score <= 10:
        controls.append(flet.Column([flet.Icon(flet.icons.CHECK_CIRCLE, color=flet.colors.GREEN), flet.Text(f"Ваш результат: {result}.\nОценка - отлично!", color=flet.colors.GREEN,)], alignment=flet.MainAxisAlignment.CENTER))
    elif 6 <= score < 8:
        controls.append(flet.Column([flet.Icon(flet.icons.MOOD, color=flet.colors.YELLOW), flet.Text(value=f"Ваш результат: {result}.\nОценка - хорошо!", color=flet.colors.YELLOW)], alignment=flet.MainAxisAlignment.CENTER))
    if 4 <= score <= 6:
        controls.append(flet.Column([flet.Icon(flet.icons.WARNING, color=flet.colors.ORANGE), flet.Text(value=f"Ваш результат: {result}.\nОценка - удовлетворительно!", color=flet.colors.ORANGE)], alignment=flet.MainAxisAlignment.CENTER))
    if score <= 4:
        controls.append(flet.Column([flet.Icon(flet.icons.CLOSE_ROUNDED, color=flet.colors.RED), flet.Text(value=f"Ваш результат: {result}.\nОценка - неудовлетворительно!", color=flet.colors.RED)], alignment=flet.MainAxisAlignment.CENTER))

    view = flet.View(f"/result/{url}", 
                     controls=controls,
                    appbar=appbar,
                    navigation_bar=navbar
                    )
    return view