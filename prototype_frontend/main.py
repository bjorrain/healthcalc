import flet as ft
from mainpage import listpage, mainpage, inputpage, outputpage
import logging

logging.basicConfig(level=logging.INFO)

def main(page: ft.Page):
    navbar = ft.NavigationBar(destinations=[
        ft.NavigationDestination(icon=ft.icons.HOME, label="Главная"),
        ft.NavigationDestination(icon=ft.icons.PEOPLE, label="Мои результаты"),
    ])
    appbar = ft.AppBar(title=ft.Text("Здоровье"), center_title=True)
    page.navigation_bar = navbar
    router = ft.TemplateRoute(page.route)
    def route_handler(e: ft.RouteChangeEvent):
        logging.info(f"page changed to {page.route}")
        page.views.clear()
        if page.route == "/":
            logging.info("Main page")
            page.views.append(mainpage(page, appbar, navbar))
        elif page.route == "/calclist":
            logging.info("List page")
            page.views.append(listpage(page, appbar, navbar))
        elif page.route == "my_results":
            # page.views.append(resultpage(page, appbar, navbar))
            ...
        else: 
            if not page.route:
                page.views.append(mainpage(page, appbar, navbar))
                return
            route = page.route.split("/")[1::]
            print(route)
            if route[0] == "calc":
                logging.info("Input page")
                print("/".join(route)[1::])
                page.views.append(inputpage(page, "/".join(route[1::]), appbar, navbar))
            elif route[0] == "result":
                logging.info("Output page")
            page.views.append(outputpage(page, "/".join(route[1::]), appbar, navbar))
        page.update()
    page.on_route_change = route_handler
    def view_pop(view):
        page.views.pop()
        top_view = page.views[-1]
        page.go(top_view.route)
    page.on_view_pop = view_pop
    page.go(page.route)



ft.app(main, port=8000, view=None)
